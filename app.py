from flask import Flask, jsonify, request
from neo4j import GraphDatabase
from dotenv import load_dotenv
import os #provides ways to access the Operating System and allows us to read the environment variables

load_dotenv()

app = Flask(__name__)

uri = os.getenv('URI')
user = os.getenv('USERN')
password = os.getenv('PASSWORD')
driver = GraphDatabase.driver(uri, auth=(user, password),database="neo4j")

def get_employees(tx, filter_params):
    query = "MATCH (m:Employee)"
    if filter_params:
            query += " WHERE "
            conditions = []
            for key, value in filter_params.items():
                conditions.append(f"m.{key} = '{value}'")
            query += " AND ".join(conditions)
    query += " RETURN m "
    results = tx.run(query).data()
    employees = [{'result': result['m']} for result in results]
    return employees

@app.route('/employees', methods=['GET'])
def get_employees_route():
    try:
        filter_params = request.args.to_dict()
        with driver.session() as session:
            employees = session.read_transaction(get_employees, filter_params=filter_params)
        
        response = {'employees': employees}
        return jsonify(response)
    except Exception as e:
        return jsonify({'error': str(e)}), 500


def add_employee(tx, employeeID, name, position, department):
    employeeID=int(employeeID)
    query = (
        "MATCH (d:Department {name: $department})<-[:MANAGES]-(manager:Employee) "
        "CREATE (manager)-[:MANAGES]->(e:Employee {employeeID: $employeeID, name: $name, position: $position})-[:WORKS_IN]->(d)"
    )
    tx.run(query, employeeID=employeeID, name=name, position=position, department=department)


def check_uniqueness(tx, name):
    query = "MATCH (m:Employee {name: $name}) RETURN COUNT(m) AS count"
    result = tx.run(query, name=name).single()
    return result['count'] == 0

@app.route('/employees', methods=['POST'])
def add_employee_route():
    
    name = request.args.get('name')
    position = request.args.get('position')
    employeeID = request.args.get('employeeID')
    department = request.args.get('department')
    with driver.session() as session:
        if not (name==None or employeeID == None or department == None):
            if session.read_transaction(check_uniqueness, name):
                session.write_transaction(add_employee, employeeID, name, position, department)
                response = {'status': 'success'}
            else:
                response = {'status': 'Halted - That person exists'}
        else:
            response = {'status': 'Missing parameters'}
    return jsonify(response)


def update_employee(tx, employeeID, new_name, new_position, new_department):
    query_match = "MATCH (m:Employee) WHERE m.employeeID=$employeeID RETURN m"
    result = tx.run(query_match, employeeID=employeeID).data()

    if not result:
        return None
    else:
        query_update = "MATCH (m:Employee {employeeID: $employeeID})"
        set_clauses = []

        if not new_name == None:
            set_clauses.append("m.name = $new_name")
        if not new_position == None:
            set_clauses.append("m.position = $new_position")
        if len(set_clauses) > 0:
            query_update += " SET " + ", ".join(set_clauses)

        tx.run(query_update, employeeID=employeeID, new_name=new_name, new_position=new_position)

        if not new_department == None:
            query_delete_relationships = "MATCH (e:Employee {employeeID: $employeeID})-[r]-() DELETE r"
            tx.run(query_delete_relationships, employeeID=employeeID)

            query_create_relationships = (
                "MATCH (manager:Employee)-[:MANAGES]->(d:Department{name:$new_department}),"
                "(e:Employee {employeeID: $employeeID}) CREATE (e)-[:WORKS_IN]->(d), "
                "(manager)-[:MANAGES]->(e)"
            )
            tx.run(query_create_relationships, employeeID=employeeID, new_department=new_department)

        return {'Update Complete'}
@app.route('/employees/<int:employeeID>', methods=['PUT'])
def update_employee_route(employeeID):
    new_name = request.args.get('new_name') 
    new_position = request.args.get('new_position') 
    new_department = request.args.get('new_department')
    
    with driver.session() as session:
        print(employeeID, new_department, new_name, new_position)
        employee = session.write_transaction(update_employee, employeeID=employeeID, new_name=new_name, new_position=new_position, new_department=new_department)

    if not employee:
        response = {'message': 'Employee not Found'}
        return jsonify(response), 404
    else:
        response = {'status': 'success'}
        return jsonify(response)


def delete_employee(tx, employeeID):
    query = "MATCH (e:Employee) WHERE e.employeeID=$employeeID RETURN e"
    result = tx.run(query, employeeID=employeeID).data()

    if not result:
        return None
    else:
        isManager = "MATCH(e:Employee)-[:MANAGES]->(d:Department) WHERE e.employeeID=$employeeID DETACH DELETE d"
        tx.run(isManager, employeeID=employeeID)
        query = "MATCH (e:Employee) WHERE e.employeeID=$employeeID DETACH DELETE e"
        tx.run(query, employeeID=employeeID)
        return {'employeeID': employeeID}

@app.route('/employees/<int:employeeID>', methods=['DELETE'])
def delete_employee_route(employeeID):
    with driver.session() as session:
        employee = session.write_transaction(delete_employee, employeeID)

    if not employee:
        response = {'message': 'Employee not found'}
        return jsonify(response), 404
    else:
        response = {'status': 'success'}
        return jsonify(response)


def find_subordinates(tx, employeeID):
    query = "MATCH (e:Employee) WHERE e.employeeID=$employeeID RETURN e"
    result = tx.run(query, employeeID=employeeID).data()

    if not result:
        return None
    else:
        query = "MATCH(m:Employee)-[:MANAGES]->(e:Employee) WHERE m.employeeID=$employeeID return e"
        results = tx.run(query, employeeID=employeeID)
        employees = [{'result': dict(result['e'])} for result in results]
        return employees


@app.route('/employees/<int:employeeID>/subordinates', methods=['GET'])
def find_subordinates_route(employeeID):
    with driver.session() as session:
        subordinates = session.write_transaction(find_subordinates, employeeID)

    if not subordinates:
        response = {'message': 'Employee not found or is not a manager'}
        return jsonify(response), 404
    else:
        response = {'subordinates': subordinates}
        return jsonify(response)


def find_department_employees(tx, departmentID):
    query = "MATCH (d:Department) WHERE d.departmentID=$departmentID RETURN d"
    result = tx.run(query, departmentID=departmentID).data()

    if not result:
        return None
    else:
        query = "MATCH (d:Department)<-[rel:WORKS_IN]-(employees:Employee) WHERE d.departmentID=$departmentID RETURN employees"
        results = tx.run(query, departmentID=departmentID)
        employees = [{'result': dict(result['employees'])} for result in results]
        return employees


@app.route('/employees/<int:departmentID>/employees', methods=['GET'])
def find_department_employees_route(departmentID):
    with driver.session() as session:
        employees = session.write_transaction(find_department_employees, departmentID)

    if not employees:
        response = {'message': 'Department not found'}
        return jsonify(response), 404
    else:
        response = {'employees': employees}
        return jsonify(response)


def get_department_info(tx, employeeID):
    query = """
    MATCH (e:Employee {employeeID: $employeeID})-[:WORKS_IN]->(d:Department)
    OPTIONAL MATCH (d)<-[:WORKS_IN]-(employees:Employee)
    OPTIONAL MATCH (d)<-[:MANAGES]-(manager:Employee)
    RETURN
      d.name AS departmentName,
      COUNT(employees) AS numberOfEmployees,
      manager.name AS departmentManager
    """
    result = tx.run(query, employeeID=employeeID).data()
    return result[0] if result else None


@app.route('/employees/<int:employeeID>/departmentInfo', methods=['GET'])
def get_department_info_route(employeeID):
    with driver.session() as session:
        department_info = session.read_transaction(get_department_info, employeeID)

    if not department_info:
        response = {'message': 'Employee not found'}
        return jsonify(response), 404
    else:
        response = {
            'departmentName': department_info['departmentName'],
            'numberOfEmployees': department_info['numberOfEmployees'],
            'departmentManager': department_info['departmentManager']
        }
        return jsonify(response)


def get_departments(tx, filter_by, filter_value, sort_by, sort_order):
    query = "MATCH (d:Department) "

    if filter_by and filter_value:
        query += f" WHERE ANY(x IN [d.{filter_by}] WHERE x CONTAINS '{filter_value}') "
    query+=" RETURN d "
    if sort_by:
        if sort_order==None :
            sort_order = "asc"
        query += f" ORDER BY d.{sort_by} {sort_order}"

    results = tx.run(query).data()

    departments = [{'name': result['d']['name']} for result in results]
    return departments


@app.route('/departments', methods=['GET'])
def get_departments_route():
    try:

        filter_by = request.args.get('filter_by')
        filter_value = request.args.get('filter_value')
        sort_by = request.args.get('sort_by')
        sort_order = request.args.get('sort_order')
    
        with driver.session() as session:
            departments = session.read_transaction(get_departments,filter_by, filter_value, sort_by, sort_order)

        response = {'departments': departments}
        return jsonify(response)
    except Exception as e:
        return jsonify({'error': str(e)}), 500

if __name__ == '__main__':
    app.run()